package com.bricknbolt.questionsModule.enums;

public enum QuestionInputType {
    DROPDOWN, TEXT, RADIO, RATING, ATTACHMENT, QUESTIONS_LIST;
}
