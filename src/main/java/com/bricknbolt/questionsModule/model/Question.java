package com.bricknbolt.questionsModule.model;

import com.bricknbolt.questionsModule.enums.QuestionInputType;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@Document( collection = "feedbackQuestions" )
public class Question {
    @Id
    private String id;

    private String questionKey;
    private String questionValue;

    private Boolean isActive;

    private QuestionInputType inputType;
//  Question properties
//  Dropdown or radio options
    private List<String> options;
//  eg - minValue or maxValue of a rating
    private Integer minValue;
    private Integer maxValue;

//  question having multiple questions
    private List<String> subQuestionsIds;

}
