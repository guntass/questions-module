package com.bricknbolt.questionsModule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionModule {

	public static void main(String[] args) {
		SpringApplication.run(QuestionModule.class, args);
	}
}





